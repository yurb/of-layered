#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main(int argc, char* argv[]){
	ofSetupOpenGL(VW * GLOBAL_SCALE,
                  VH * GLOBAL_SCALE,
                  OF_GAME_MODE);			// <-------- setup the GL context
    ofDisableDepthTest();
    ofSetFrameRate(24);

    if (argc < 2) {
        printf("You fool!");
        exit(1);
    }
    std::string videoPath = argv[1];

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new ofApp(videoPath));

}
