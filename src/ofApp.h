#pragma once

#define VW 1224
#define VH 1428
//#define GLOBAL_SCALE 0.6
//#define GLOBAL_SCALE 1
#define GLOBAL_SCALE (1248.0 / 1224.0)

#define NUM_SECTIONS 3
#define SECTION_HEIGHT (VH / NUM_SECTIONS)

// OSC listening port
#define OSC_PORT 17225

#include "ofxOsc.h"
#include "ofMain.h"


class OnePoleLPF {
public:
    OnePoleLPF(float initial, float alpha = 0.1);
    void update();

    float input = 0;
    float output = 0;
    float alpha = 0.1;
};


class VideoTrack {
public:
    VideoTrack(ofDirectory trackDir);

    void next();
    void reset();
    float getRemainingSec();

    void update();
    void draw(ofTexture &mask, int rLevel, int gLevel, int bLevel);

    // Sequencing
    int curFile = 0;
    int nextFile = 0;
    float transition = 0;
    float transitionDur = 4;

    // Drawing
    int sectionPos = 0;
    int sectionHeight = NUM_SECTIONS;
    float opacity = 1;
    float scale = 1;
    ofBlendMode blendMode = OF_BLENDMODE_ALPHA;

private:
    // Poll of video players
    std::vector<ofVideoPlayer> players;
};

class ofApp : public ofBaseApp{

public:
    ofApp(const std::string &videoPath)
        : videoPath(videoPath)
    {
    }

    void drawRhombus(float size, float alpha);
    void reset();

    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    std::string videoPath;
    std::vector<VideoTrack> tracks;

    OnePoleLPF rhombusSize {150};
    OnePoleLPF rhombusAlpha {0, 0.05};

    // Color filtering
    OnePoleLPF rLevel {255};
    OnePoleLPF gLevel {255};
    OnePoleLPF bLevel {255};

    ofxOscReceiver receiver;
    ofFbo fbo;
};
