#include "ofApp.h"

const int stepY = VH / NUM_SECTIONS;


//--------------------------------------------------------------
OnePoleLPF::OnePoleLPF(float initial, float alpha)
    : input(initial), output(initial), alpha(alpha)
{
}

void OnePoleLPF::update() {
    output = (alpha * input) + (1 - alpha) * output;
}

//--------------------------------------------------------------
VideoTrack::VideoTrack(ofDirectory trackDir) {
    trackDir.allowExt("avi");
    trackDir.listDir();
    trackDir.sort();

    // Initialize players with files
    for (const ofFile &file : trackDir) {
        if (!file.isDirectory()) {
            ofVideoPlayer player;
            player.load(file.getAbsolutePath());
            player.setLoopState(OF_LOOP_NONE);
            player.stop();
            this->players.push_back(std::move(player));
            ofLog() << file.getAbsolutePath();
        } else {
            ofLogWarning() << "Skipping directory: " << file.getAbsolutePath();
        }
    }
    // If we end up with only one file, duplicate it to have it crossfade into itself
    if (players.size() == 1) {
        ofVideoPlayer player;
        player.load(players[0].getMoviePath());
        player.stop();
        players.push_back(std::move(player));
    }
}
float VideoTrack::getRemainingSec() {
    return players[curFile].getDuration() * (1 - players[curFile].getPosition());
}

void VideoTrack::update() {
    {
        auto &player = players[curFile];
        if (!player.isPlaying()) {
            ofLogNotice() << "Starting player: " << player.getMoviePath();
            player.play();
        }
        player.update();
    }

    if (curFile != nextFile) {
        auto &player = players[nextFile];
        if (!player.isPlaying()) {
            ofLogNotice() << "Starting player: " << player.getMoviePath();
            player.play();
        }
        player.update();

        transition = 1 - (getRemainingSec() / transitionDur);
        if (players[curFile].getIsMovieDone()) {
            ofLogNotice() << "Stopping player: " << player.getMoviePath();
            players[curFile].setPosition(0);
            players[curFile].stop();
            curFile = nextFile;
            transition = 0;
        }
    }

    if (curFile == nextFile) {
        auto &player = players[curFile];
        if (getRemainingSec() <= transitionDur) {
            this->next();
        }
    }
}
void VideoTrack::next() {
    transition = 0;
    ofLogNotice() << ">>> NEXT <<<";
    nextFile = (nextFile + 1) % players.size();
}

void VideoTrack::draw(ofTexture &mask, int rLevel, int gLevel, int bLevel) {
    ofPushMatrix();
    float hpos = sectionPos * SECTION_HEIGHT;
    if (scale != 1) {
        glm::vec2 pivot(VW * 0.5, (VH * 0.5) + hpos);
        ofTranslate(pivot);
        ofScale(scale);
        ofTranslate(-pivot);
    }

    ofEnableBlendMode(blendMode);
    ofSetColor(rLevel, gLevel, bLevel, ofClamp((1 - transition) * opacity * 255, 0, 255));
    players[curFile].getTexture().setAlphaMask(mask);
    players[curFile].draw(0, hpos);
    if (nextFile != curFile) {
        int alpha = ofClamp(transition * opacity * 255, 0, 255);
        ofSetColor(rLevel, gLevel, bLevel, alpha);
        players[nextFile].getTexture().setAlphaMask(mask);
        players[nextFile].draw(0, hpos);
    }
    ofSetColor(255, 255, 255, 255);
    ofPopMatrix();
}

void VideoTrack::reset() {
    curFile = 0;
    nextFile = 0;
    transition = 0;
    for (ofVideoPlayer &player : players) {
        player.setPosition(0);
        player.stop();
    }
}

//--------------------------------------------------------------
void ofApp::drawRhombus(float size, float alpha) {
    fbo.begin();
    ofClear(255 - alpha, 255 - alpha, 255 - alpha, 255);
    ofFill();
    ofPushMatrix();
    ofTranslate(VW / 2, VH / 2);
    ofRotateDeg(45);
    ofSetColor(255, 255, 255, 255);
    ofFill();
    ofDrawRectangle(-size, -size, size * 2, size * 2);
    ofPopMatrix();
    fbo.end();
}

void ofApp::setup(){
    // Dynamic content
    ofDirectory trackPaths(this->videoPath);
    trackPaths.listDir();
    trackPaths.sort();
    for (const ofFile &file : trackPaths) {
        if (file.isDirectory()) {
            ofDirectory dir(file.getAbsolutePath());
            VideoTrack track(dir);
            this->tracks.push_back(std::move(track));
        }
    }

    // Masks
    fbo.allocate(VW, VH, GL_RGBA);

    // init OSC
    receiver.setup(OSC_PORT);
}

void ofApp::update() {
    for (VideoTrack &track : this->tracks) {
        track.update();
    }

    // Handle OSC
    while (receiver.hasWaitingMessages()) {
        ofxOscMessage m;
        receiver.getNextMessage(m);

        // Opacity
        if (m.getAddress() == "/track/opacity") {
            int track = m.getArgAsInt(0);
            float opacity = m.getArgAsFloat(1);
            if (track < tracks.size())
                tracks.at(track).opacity = opacity;
        }

        if (m.getAddress() == "/track/position") {
            int track = m.getArgAsInt(0);
            int position = m.getArgAsInt(1);
            if (track < tracks.size())
                tracks.at(track).sectionPos = ofClamp(position, -1, NUM_SECTIONS - 1);
        }

        if (m.getAddress() == "/track/scale") {
            int track = m.getArgAsInt(0);
            float scale = m.getArgAsFloat(1);
            if (track < tracks.size())
                tracks.at(track).scale = ofClamp(scale, 0, 1);
        }

        if (m.getAddress() == "/rhombus/opacity") {
            float opacity = m.getArgAsFloat(0);
            rhombusAlpha.input = ofClamp(opacity * 255, 0, 255);
        }

        if (m.getAddress() == "/rhombus/size") {
            float size = m.getArgAsFloat(0);
            rhombusSize.input = size;
        }

        // Color filter
        if (m.getAddress() == "/color/red") {
            float value = m.getArgAsFloat(0);
            rLevel.input = ofClamp(value, 0, 255);
        }

        if (m.getAddress() == "/color/green") {
            float value = m.getArgAsFloat(0);
            gLevel.input = ofClamp(value, 0, 255);
        }

        if (m.getAddress() == "/color/blue") {
            float value = m.getArgAsFloat(0);
            bLevel.input = ofClamp(value, 0, 255);
        }

        if (m.getAddress() == "/all/reset") {
            reset();
        }
    }

    // Smooth parameters
    rhombusSize.update();
    rhombusAlpha.update();
    rLevel.update();
    gLevel.update();
    bLevel.update();
}

void ofApp::draw(){
    ofBackground(0);
    ofPushMatrix();
    ofScale(GLOBAL_SCALE, GLOBAL_SCALE);

    drawRhombus(rhombusSize.output, rhombusAlpha.output);
    for (VideoTrack &track : this->tracks) {
        if (track.opacity > 0)
            track.draw(fbo.getTexture(),
                       rLevel.output,
                       gLevel.output,
                       bLevel.output);
    }

    ofPopMatrix();
}

void ofApp::reset() {
    for (VideoTrack &track : tracks) {
        track.reset();
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == OF_KEY_F1)
        reset();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
